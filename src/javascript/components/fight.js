import { controls } from '../../constants/controls';


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const firstFighterDefense = firstFighter.defense;
    const secondFighterDefense = secondFighter.defense;

    firstFighter.opportunityToAttack = true;
    secondFighter.opportunityToAttack = true;

    firstFighter.criticalHit = true;
    secondFighter.criticalHit = true;

    firstFighter.defense = 0;
    secondFighter.defense = 0;

    addEventListener('keydown', onePressDown);
    addEventListener('keyup', onePressUp)

    function onePressDown(e) {
      if(e.code === controls.PlayerOneBlock) {
        firstFighter.defense = firstFighterDefense;
        firstFighter.opportunityToAttack = false;
      }
      if(e.code === controls.PlayerOneAttack && firstFighter.opportunityToAttack) {
        secondFighter.health -= getDamage(firstFighter.attack, secondFighter.defense);
      }
    }

    function onePressUp(e) {
      if(e.code === controls.PlayerOneBlock) {
        firstFighter.defense = 0;
        firstFighter.opportunityToAttack = true;
      }
    }

    addEventListener('keydown', twoPressDown);
    addEventListener('keyup', twoPressUp)

    function twoPressDown(e) {
      if(e.code === controls.PlayerTwoBlock) {
        secondFighter.defense = secondFighterDefense;
        secondFighter.opportunityToAttack = false;
      }
      if(e.code === controls.PlayerTwoAttack && twoOpportunityToAttack) {
        firstFighter.health -= getDamage(secondFighter.attack, firstFighter.defense);
      }
    }

    function twoPressUp(e) {
      if(e.code === controls.PlayerTwoBlock) {
        secondFighter.defense = 0;
        secondFighter.opportunityToAttack = true;
      }
    }


    function runOnKeys(func, ...codes) {
      let pressed = new Set();

      addEventListener('keydown', function(e) {
        pressed.add(e.code);

        for (let code of codes) {
          if (!pressed.has(code)) {
            return;
          }
        }

        pressed.clear();

        func();
      });

      addEventListener('keyup', function(e) {
        pressed.delete(e.code);
      });

    }

    runOnKeys(() => {
      if(firstFighter.criticalHit === true) {
        secondFighter.health -= getDamage(firstFighter.attack * 2, secondFighter.defense);
        firstFighter.criticalHit = false;
        setTimeout(()=>firstFighter.criticalHit = true, 10000, firstFighter);
      }
    }, ...controls.PlayerOneCriticalHitCombination);

    runOnKeys(() => {
      if(secondFighter.criticalHit === true) {
        firstFighter.health -= getDamage(secondFighter.attack * 2, firstFighter.defense);
        secondFighter.criticalHit = false;
        setTimeout(()=>secondFighter.criticalHit = true, 10000, secondFighter);
      }
    }, ...controls.PlayerTwoCriticalHitCombination);


    if (firstFighter.health === 0) {
      resolve(secondFighter);
    }

    if (secondFighter.health === 0) {
      resolve(firstFighter);
    }

  });
}

export function getDamage(attacker, defender) {
  // return damage
  return attacker.attack > defender.defense ? getHitPower(attacker) - getBlockPower(defender) : 0;

}

export function getHitPower(fighter) {
  // return hit power
  const randomNumber = Math.random() * (2 - 1) + 1;
  return fighter.attack * randomNumber;
}

export function getBlockPower(fighter) {
  // return block power
  const randomNumber = Math.random() * (2 - 1) + 1;
  return fighter.defense * randomNumber;
}
