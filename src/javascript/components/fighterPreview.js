import { createElement } from '../helpers/domHelper';
import { showModal } from './modal/modal';
import { createFighters } from './fightersView';
import { renderArena } from './arena';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  const image = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes: { src: fighter.source },
  });

  const name = createElement({
    tagName: 'h2',
  });

  name.innerHTML = fighter.name;

  const health = createElement({
    tagName: 'h2',
  });

  health.innerHTML = fighter.health;

  const attack = createElement({
    tagName: 'h2',
  });

  attack.innerHTML = fighter.attack;

  const defense = createElement({
    tagName: 'h2',
  });

  defense.innerHTML = fighter.defense;

  fighterElement.append(image, name, health, attack, defense);
  

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

